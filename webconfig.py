from pathlib import Path


class _Config:
    PROCESSING_IMAGEFOLDER = Path("Uploaded_Images")
    PROCESSING_RESULTFILENAME = "annotation.yml"
    PROCESSING_RESULTPATH = ""  # This will be created on finalize

    PROCESSING_MODEL = "models/model-V1"

    def finalize(self):
        self.PROCESSING_RESULTPATH = self.PROCESSING_IMAGEFOLDER / self.PROCESSING_RESULTFILENAME


config = _Config()
