# First the Model (in our case most likely a checkpoint) needs to be exported as TF Model
# from FRCNN.tensorpack.tfutils.export import ModelExporter
# ModelExporter(predcfg).export_serving("models\\tf\\model-V1")

# We need to support the TF_OPS in TF Lite, so we convert them with those options.
# When executing the TFLITE file, the TF Lite Runtime needs to include the necessary Ops.
# implementation 'org.tensorflow:tensorflow-lite-select-tf-ops:0.0.0-nightly-SNAPSHOT'

# in addition to
# implementation 'org.tensorflow:tensorflow-lite:0.0.0-nightly-SNAPSHOT'


import tensorflow as tf

converter = tf.lite.TFLiteConverter.from_saved_model("..\\models\\tf\\model-V1")
converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS, tf.lite.OpsSet.SELECT_TF_OPS]

tflite_model = converter.convert()

# Save the model.
with open('..\\models\\model-V1.tflite', 'wb') as f:
  f.write(tflite_model)
