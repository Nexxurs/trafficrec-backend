import cv2
import numpy as np

from .dataset import register_sign_prediction
from FRCNN.config import config as cfg, finalize_configs
from .modeling.generalized_rcnn import ResNetC4Model, ResNetFPNModel
from .tensorpack.predict import PredictConfig, OfflinePredictor
from .tensorpack.tfutils import SmartInit
from .eval import predict_image
from .viz import draw_final_outputs
import FRCNN.tensorpack.utils.viz as tpviz


def initialize(modelpath, classespath):
    """First call initialize, then initialize the predictor"""
    # cfg.freeze(False)
    register_sign_prediction(classespath)
    finalize_configs(is_training=False)
    cfg.freeze(False)
    p = Predictor(modelpath)
    cfg.freeze(True)
    return p


class Predictor:
    def __init__(self, modelpath):
        MODEL = ResNetFPNModel() if cfg.MODE_FPN else ResNetC4Model()

        predcfg = PredictConfig(
            model=MODEL,
            session_init=SmartInit(modelpath),
            input_names=MODEL.get_inference_tensor_names()[0],
            output_names=MODEL.get_inference_tensor_names()[1])

        self.pred_function = OfflinePredictor(predcfg)

    def predict(self, input_file):
        img = cv2.imread(str(input_file), cv2.IMREAD_COLOR)
        return Prediction(img, predict_image(img, self.pred_function))


class Prediction:
    def __init__(self, img, detection_result):
        self.img = img
        self.detectionResult = detection_result
        self.viz = None

    def to_output(self):
        output = []
        for detection in self.detectionResult:
            r = {
                "box": [int(x) for x in detection.box],  # todo cant serialize an ndarray!
                "score": int(detection.score * 100),
                "class": cfg.DATA.CLASS_NAMES[detection.class_id]
            }
            output.append(r)
        return {"predictions": output}

    def _get_viz(self):
        if self.viz is None:
            final = draw_final_outputs(self.img, self.detectionResult)
            self.viz = np.concatenate((self.img, final), axis=1)

        return self.viz

    def show(self):
        tpviz.interactive_imshow(self._get_viz())

    def save(self, filename):
        cv2.imwrite(filename, self._get_viz())