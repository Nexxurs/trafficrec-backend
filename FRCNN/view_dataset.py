import random
from pathlib import Path
from dataset import signs, register_sign, register_sign_filtered
import config as cfg

from viz import draw_annotation
from tensorpack.utils.viz import interactive_imshow as imshow
import cv2


class DatasetViewer:
    def __init__(self, basedir, nameprefix="GTSDB", ds_type="all", filter_list=None):
        print("Loading Dataset")

        register_sign(basedir, nameprefix)

        if filter_list:
            self.roidbs = signs.FilteredSignSplit(basedir, ds_type, filter_list).training_roidbs()
        else:
            self.roidbs = signs.SignSplit(basedir, ds_type).training_roidbs()

        print("#images:", len(self.roidbs))

        self.name_dict = {}
        for roi in self.roidbs:
            p = Path(roi['file_name'])
            self.name_dict[p.name] = roi

        cfg.finalize_configs(False)

    def show_gt(self, img_name):
        r = self.name_dict[img_name]

        im = cv2.imread(r["file_name"])
        vis = draw_annotation(im, r["boxes"], r["class"])
        imshow(vis)

    def show_all(self):
        for r in self.roidbs:
            im = cv2.imread(r["file_name"])
            vis = draw_annotation(im, r["boxes"], r["class"])
            imshow(vis)

    def show_random(self):
        r = random.choice(self.roidbs)

    def _show(self, roidb):
        im = cv2.imread(roidb["file_name"])
        vis = draw_annotation(im, roidb["boxes"], roidb["class"])
        imshow(vis)



def create_filter(filter_path = Path("../Dataset/GTSDB/filter.txt")):
    filter_list = []
    with open(filter_path, "r") as file:
        for line in file.readlines():
            line = line.strip()
            filter_list.append(f"{line}.png")
    return filter_list


if __name__ == '__main__':
    viewer = DatasetViewer("..\\Dataset\\GTSDB")
    # viewer.show_all()
