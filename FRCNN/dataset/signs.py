import numpy as np

from .dataset import DatasetSplit, DatasetRegistry
from pathlib import Path
from FRCNN.config import config as cfg

__all__ = ["register_sign", "register_sign_filtered", "register_sign_prediction"]

USE_QUARTER = lambda x: x % 4 == 0


class SignSplit(DatasetSplit):
    def __init__(self, base_dir, split):
        assert split in ["train", "val", "all"]
        if not isinstance(base_dir, Path):
            base_dir = Path(base_dir)
        assert base_dir.exists()
        self.base_dir = base_dir

        if split == "train":
            self.use_index = lambda x: not USE_QUARTER(x)
        elif split == "val":
            self.use_index = USE_QUARTER
        else:
            self.use_index = lambda x: True

    def training_roidbs(self):

        ret = []

        with open(self.base_dir / "gt.txt", "r") as file:
            last_elem = None
            last_elem_rois = None
            last_elem_classes = None
            last_elem_index = 0

            for line in file.readlines():
                line_meta = self._parse_line(line)

                if last_elem is None:
                    last_elem = line_meta
                    last_elem_rois = [line_meta['roi']]
                    last_elem_classes = [line_meta['class_id']]
                    last_elem_index += 1
                    continue

                if last_elem['file_name'] == line_meta['file_name']:
                    last_elem_rois.append(line_meta['roi'])
                    last_elem_classes.append(line_meta['class_id'])
                else:
                    # finish last element
                    if self.use_index(last_elem_index):
                        roidb = self._meta_to_roidb(last_elem, last_elem_rois, last_elem_classes)
                        ret.append(roidb)

                    last_elem = line_meta
                    last_elem_rois = [line_meta['roi']]
                    last_elem_classes = [line_meta['class_id']]
                    last_elem_index += 1

            # after foreach line: parse last image
            if last_elem is None:
                return ret
            if self.use_index(last_elem_index):
                roidb = self._meta_to_roidb(last_elem, last_elem_rois, last_elem_classes)
                ret.append(roidb)
            return ret

    def _meta_to_roidb(self, elem, rois, classes):
        roidb = {
            "file_name": str(elem['file_path']),
            "boxes": np.asarray(rois, dtype=np.float32),
            # "segmentation": [],  # Only include when Masked R-CNN
            "class": np.asarray(classes, dtype=np.int32),
            "is_crowd": np.zeros((len(classes),), dtype=np.int8)
        }
        return roidb

    def _parse_line(self, line):
        line = line.strip()

        # 0 .. Filename
        # 1 .. Roi.X1
        # 2 .. Roi.Y1
        # 3 .. Rox.X2
        # 4 .. Rox.Y2
        # 5 .. ClassId (from original Dataset --> starts with 0)
        file_args = line.split(";")

        result = {}

        file_name = file_args[0]
        if file_name.endswith(".ppm"):
            file_name = file_name.replace(".ppm", ".png")

        result['file_name'] = file_name

        result['class_id'] = int(file_args[5]) + 1

        roi = (file_args[1], file_args[2], file_args[3], file_args[4])
        result['roi'] = [int(x) for x in roi]

        img_path = self.base_dir / file_name
        img_path = img_path.resolve().absolute()
        assert img_path.exists()
        result['file_path'] = img_path

        return result


class FilteredSignSplit(SignSplit):
    def __init__(self, base_dir, split, filter):
        super().__init__(base_dir, split)
        self.filter = filter

    def training_roidbs(self):
        all_elems = super(FilteredSignSplit, self).training_roidbs()
        res = []
        for elem in all_elems:
            p = Path(elem["file_name"])
            if p.name in self.filter:
                res.append(elem)
        return res


def register_sign_filtered(base_dir, nameprefix, filter):
    class_names = ["BG"]

    # with open(os.path.join(base_dir, "Classes.txt"), "r") as file:
    with open(cfg.DATA.SIGNS.CLASSNAMES_PATH, "r") as file:
        for line in file.readlines():
            line = line.strip()
            class_id, name = line.split(":")
            class_names.append(name)

    for split in ["train", "val"]:
        name = f"{nameprefix}_{split}"
        DatasetRegistry.register(name, lambda x=split: FilteredSignSplit(base_dir, x, filter))
        DatasetRegistry.register_metadata(name, "class_names", class_names)


def register_sign_prediction(classes_file):
    class_names = ["BG"]

    # with open(os.path.join(base_dir, "Classes.txt"), "r") as file:
    with open(classes_file, "r") as file:
        for line in file.readlines():
            line = line.strip()
            class_id, name = line.split(":")
            class_names.append(name)

    # The following name 'GTSDB_train' is not arbitrary. On Finalization of the config file , the class names
    # will be read from the first training dataset configured in the config. So we just populate that names Dataset
    # with the correct class names
    DatasetRegistry.register_metadata("GTSDB_train", "class_names", class_names)


def register_sign(base_dir, nameprefix):
    class_names = ["BG"]

    # with open(os.path.join(base_dir, "Classes.txt"), "r") as file:
    with open(cfg.DATA.SIGNS_CLASSNAMES_PATH, "r") as file:
        for line in file.readlines():
            line = line.strip()
            class_id, name = line.split(":")
            class_names.append(name)

    for split in ["train", "val"]:
        name = f"{nameprefix}_{split}"
        DatasetRegistry.register(name, lambda x=split: SignSplit(base_dir, x))
        DatasetRegistry.register_metadata(name, "class_names", class_names)