REM Use the following line to extend the PATH ENV variable with the correct position of CUDNN
REM (This assumes the binary files from CUDNN/bin are integratged into CUDA/bin)
SET PATH=%PATH%;C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.2\bin\;
pip3 install -r requirements.txt
python main.py