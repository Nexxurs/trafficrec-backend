# Requirements

bin directories of CUDA v11.2 and CUDNN compatible with that version (eg. 8.9.2 for 11.X) have to be in the PATH. 
An example of a correct PATH environment variable is here:
 > Path=C:\Windows\system32\;C:\Windows\;C:\Windows\System32\Wbem\;C:\Users\user\AppData\Local\Microsoft\WindowsApps\;C:\Program Files\dotnet\\;C:\WINDOWS\system32\;C:\WINDOWS\;C:\WINDOWS\System32\Wbem\;C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.2\bin\;C:\Program Files\NVIDIA GPU Computing Toolkit\CUDNN\v11.2\bin\;


To setup the python interpreter, run 

 > pip install -r requirements.txt


## cocotools
This requirement is needed to work on Coco Datasets (which I used as a base), but it may be difficult to install.

If an error is thrown, use the link provided in the error and install "Desktopdevelopment with C++" from the Visual Studio Build Tools


# Run the Server
With the working directoy at the root of this directory (trafficrec-backend) run
 > python main.py
