from flask import Flask, request
from webconfig import config
from datetime import datetime
from werkzeug.utils import secure_filename
from FRCNN import initialize
import time

app = Flask(__name__)
config.finalize()
if not config.PROCESSING_IMAGEFOLDER.exists():
    config.PROCESSING_IMAGEFOLDER.mkdir()

predictor = initialize(config.PROCESSING_MODEL, "classes.txt")


def create_name(ext):
    now = datetime.now()
    if not ext.startswith("."):
        ext = "."+ext
    return now.strftime("%Y%m%d_%H%M%S")+ext


@app.route("/")
def main():
    return "Server is running!"


@app.route("/version")
def version():
    return "BA V1"


@app.route("/api/process", methods=["POST"])
def api_process():
    print("New Processing Request")
    start_time = time.perf_counter()
    data = request.files["img"]
    data_loaded_time = time.perf_counter()
    name = secure_filename(data.filename)
    name_parts = name.split(".")
    if len(name_parts) > 1:
        location = config.PROCESSING_IMAGEFOLDER / create_name(ext=name_parts[-1])
    else:
        location = config.PROCESSING_IMAGEFOLDER / create_name(ext="null")
    print(f"Saving File in {location}")
    data.save(location)

    result = predictor.predict(location)
    end_time = time.perf_counter()
    print(f"Prediction took {round((end_time - start_time)*1000)}ms "
          f"(data loading {round((data_loaded_time - start_time)*1000)}ms)")
    return result.to_output()


if __name__ == '__main__':
    app.run("0.0.0.0")