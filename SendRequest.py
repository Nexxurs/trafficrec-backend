import requests
import time

filepath_dataset = "../Backend/Dataset/Random/images.jpg"


start = time.perf_counter()

with open("Uploaded_Images/20220924_095724.jpg", "rb") as file:
    res = requests.post("http://127.0.0.1:5000/api/process", files={"img": file})

end = time.perf_counter()
print(f"Took {round((end - start)*1000)}ms")
print(res.text)